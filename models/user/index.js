const userModel = require('./user')

module.exports={

/* retreiving user details */
 async getUser(data){
    return await userModel.findOne(data)
},

/* saving user details */
 async  saveUser(data){
    return await userModel(data).save()
},

/*updating user detail*/
async updateUser(id,data){
    return await userModel.updateOne({_id:id},{firstName:data.firstName,lastName:data.lastName})
} 

}

