const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName :{type:String,required:true},
    lastName:{type:String,required:true},
    email:{type:String,require:true},
    password:{type:String}
})

module.exports = mongoose.model("user",userSchema)