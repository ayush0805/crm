const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
    userId:{type:mongoose.Schema.ObjectId,required:true},
    adminId:{type:mongoose.Schema.ObjectId,required:true},
    task:{type:String,required:true},
    isDelete:{type:Boolean,default:false},
    createdAt:{type:Date,default:Date.now()},
    updatedAt:{type:Date,default:null},
    deletedAt:{type:Date,default:null}
    
});

module.exports = mongoose.model("tasks",taskSchema);