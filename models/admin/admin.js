const mongoose = require('mongoose')

const adminSchema = new mongoose.Schema({
    firstName:{type:String},
    lastName:{type:String},
    email:{type:String},
    password:{type:String},
    userType:{type:String,default:"admin"},
    profileImage:{type:String,default:null}
})

module.exports = mongoose.model("admin",adminSchema)