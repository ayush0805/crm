const adminModel = require('./admin')

module.exports={

/*retreiving admin detail*/
async getAdmin(data){
    return await adminModel.findOne(data)
},

/*saving admin detail*/
async saveAdmin(data){
    return await adminModel(data).save()
},

async isAdminExist(id){
 return await adminModel.findOne({_id:id})
}
}