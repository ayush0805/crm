const express = require("express")
const route = express.Router();
const controller = require('../controller/user')
const authorize= require('../services/authorization')
const roles = require('../services/roles')

route.post('/signup',controller.userSignup)
route.get('/signin',controller.userSignin)
route.post('/edit-detail',authorize(roles.User),controller.editDetails)


module.exports = route;