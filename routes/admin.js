const express = require("express")
const route = express.Router();
const controller = require('../controller/admin')
const authorize = require('../services/authorization')
const roles = require('../services/roles')
const upload = require('../services/multer')

route.post('/signup',controller.adminSignup)
route.get('/signin',controller.adminSignin)
route.post('/assign-task',authorize(roles.Admin),controller.assignTasks)
route.post('/send-message/:id',controller.sendMessage)
route.post('/upload',upload.array('multi-files'),controller.uploadImage)



module.exports = route;