require("dotenv").config();
const express = require('express')
const mongoose = require("mongoose");
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
const app = express();
const port =5000;

const userRoute = require('./routes/user')
const adminRoute = require('./routes/admin')

app.use('/docs-api', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use(express.json());
app.use('/user',userRoute);
app.use('/admin',adminRoute)

//MongoDB connection
mongoose
  .connect(process.env.DB_SECRET, {})
  .then(() => console.log("Connected to Database"))
  .catch((err) => console.log(err));

const server = app.listen(port,()=>{
    console.log(`Connected successfully on port ${port}`)
})

