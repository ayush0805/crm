var firebaseAdmin = require("firebase-admin");


var serviceAccount = require("../firebaseCredentials.json");

//Initialize App
const admin=firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://mycrm-fbd97-default-rtdb.firebaseio.com"
});



module.exports= admin