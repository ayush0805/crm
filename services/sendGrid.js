const sgMail=require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_SECRET_KEY);

const sendEmail= async(ToEmail,task) =>{
    try{
        const message = {
            from: process.env.SENDGRID_FROM_EMAIL,
            to: ToEmail,
            subject:"Todays's Task",
            html: `<div>
                   Your task has been assigned to you. Please find it below:<br>
                   <p>${task}</p><br>
                   <p>For More Info Login into CRM</p>  
                   </div> `       
            };
            await sgMail.send(message);
    }catch(err){console.log("error of mail:",err)}
}
module.exports = sendEmail;