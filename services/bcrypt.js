const bcrypt = require("bcrypt");

module.exports = {
  // function for encrypt password 
  bcryptHash(value, salt) {
    const HASH_VALUE = bcrypt.hashSync(value, salt);
    return HASH_VALUE;
  },
};