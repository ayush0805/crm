const adminModule = require('../models/admin/index');
const userModule = require('../models/user/index')
const taskModule = require('../models/task/index')
const {bcryptHash}= require('../services/bcrypt');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const {ObjectId}= require('bson')
const admin = require('../services/firebase')
const nodeWebcam = require('node-webcam')
const FirebaseToken = process.env.firebaseToken
const sendmail = require('../services/sendGrid');

module.exports={

   /*admin signup*/
    async adminSignup(req,res){
        const { firstName, lastName, email, password } = req.body;
        if (!firstName) return res.status(422).send({code:422,status:'failure',message:"first name is required"});
        if (!lastName) return res.status(422).send({code:422,status:'failure',message:"last name is required"});
        if (!email) return res.status(422).send({code:422,status:'failure',message:"Email is required"});
        if (!password) return res.status(422).send({code:422,status:'failure',message:"Password is required"}); 
        try{
            let adminData = await adminModule.getAdmin({email:req.body.email})
            if(adminData) return res.status(422).send({code:422,status:"failure",message:"User already exists"});
            const hashValue = bcryptHash(password, 10);
            req.body.password = hashValue;
            data = req.body;
            let admin= await adminModule.saveAdmin(data);
            return res.status(200).send({code:200,status:'success',message:admin})
        }catch(err){
            console.log(err)
            return res.status(422).send({code:422,status:"failure",message:err.message})
        }
    },

    /*admin login*/
    async adminSignin(req,res){
        let email = req.body.email;
        let password = req.body.password
        if (!email) return res.status(422).send({code:422,status:'failure',message:"email is required"})
        if (!password) return res.status(422).send({code:422,status:'failure',message:"password is required"})
        try {
             const adminData = await adminModule.getAdmin({ email: req.body.email })
             console.log(adminData)
             if(!adminData) return res.status(422).send({code:422, status:"failure", message:"admin not found!"})
             const loginPassword = bcrypt.compareSync(req.body.password,adminData.password);
             if (!loginPassword) {
                 return res.status(200).send({code:200,status:"failure", message:"Incorrect Password"});
               }
             const token = jwt.sign({_id:adminData._id,firstName:adminData.firstName,lastName:adminData.lastName, email: adminData.email,userType:adminData.userType,roles:"Admin" }, process.env.TOKEN);
             return res.status(200).send({code:200,status :"success",token:token})
            }catch(err){
                 console.log(err)
                 return res.status(422).send({code:422,status :"failure",message:err.message})
             }
    },
    
    /*admin assigning task*/
    async assignTasks(req,res){
        let token = req.headers.authorization
        let userId = req.body.userId
        let task = req.body.task
        if(!token ||!userId ||!task ) return res.status(422).send({code:422,status:'failure',message:"Missing Data"});
        try{
            let userExist = await userModule.getUser(ObjectId(userId))
            if(!userExist) return res.status(422).send({code:422,status:'failure',message:"User Not found"});
            let data = {
                userId:userExist._id,
                adminId:req.user._id,
                task:req.body.task
            }
            let taskassign = await taskModule.taskAssign(data)
            let mail = await sendmail(userExist.email,data.task)
            return res.status(200).send({code:200,status:'success',message:"Task has been assigned successfully"})
        }catch(err){
            console.log(err)
            return res.status(422).send({code:422,status:'failure',message:err.message})
        }
    },

    /*Send message Hi if request paramter is id=1*/
    async sendMessage(req,res){
        let id = req.params.id
        if(!id) return res.status(422).send({code:422,status:'failure',message:"Missing Data"})
        try{
            if(id==1){ 
                return res.send({message:"Hi"})
            }
            return res.status(422).send({code:422,status:'failure',message:"Something went Wrong"})
        }catch(err){
            return res.status(422).send({code:422,status:'failure',message:err.message})
        }
    },

    /*upload multiple image in one go*/
    async uploadImage(req,res){
        try{
            res.status(200).send({code:200,status:"success",message:"Upload Successfully"})
        }catch(err){
            console.log(err)
            return res.status(422).send({code:422,status:'failure',message:err.message})
        }
    },

    // async notification(req,res){
    //     try{
    //         const message = {
    //              notification:{
    //                 title: "New Message",
    //                 body: "Testing for push notification",
    //              },
    //              token: FirebaseToken
    //         }
    //         let sendNotification=await sendPushNotification(message)
          
    //         console.log(sendNotification)
    //     }catch(err){
    //         console.log(err)
    //         return res.status(422).send({code:422,status:'failure',message:err.message})
    //     }
    // },

  
}

function sendPushNotification(message){
  admin.messaging().send(message)
  .then((response)=>{
      console.log("Successfully sent message",response)
  }).catch(err=>{
      console.log('Error sending message',err)
  })
}