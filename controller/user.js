const userModule = require('../models/user/index');
const {bcryptHash}= require('../services/bcrypt');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
module.exports={
    async userSignup(req,res){
        const { firstName, lastName, email, password } = req.body;
        if (!firstName) return res.status(422).send({code:422,status:'failure',message:"first name is required"});
        if (!lastName) return res.status(422).send({code:422,status:'failure',message:"last name is required"});
        if (!email) return res.status(422).send({code:422,status:'failure',message:"Email is required"});
        if (!password) return res.status(422).send({code:422,status:'failure',message:"Password is required"}); 
        try{
            let userData = await userModule.getUser({email:req.body.email})
            if(userData) return res.status(422).send({code:422,status:"failure",message:"User already exists"});
            const hashValue = bcryptHash(password, 10);
            req.body.password = hashValue;
            data = req.body;
            let user= await userModule.saveUser(data);
            return res.status(200).send({code:200,status:'success',message:user})
        }catch(err){
            return res.status(422).send({code:422,status:"failure",message:err.message})
        }
    },
    async userSignin(req,res){
        let email = req.body.email;
        let password = req.body.password
        if (!email) return res.status(422).send({code:422,status:'failure',message:"email is required"})
        if (!password) return res.status(422).send({code:422,status:'failure',message:"password is required"})
        try {
             const userData = await userModule.getUser({ email: req.body.email })
             if(!userData) return res.status(422).send({code:422, status:"failure", message:"user not found!"})
             const loginPassword = bcrypt.compareSync(req.body.password,userData.password);
             if (!loginPassword) {
                 return res.status(200).send({code:200,status:"failure", message:"Incorrect Password"});
               }
             const token = jwt.sign({_id:userData._id,firstName:userData.firstName,lastName:userData.lastName, email: userData.email,roles:"User" }, process.env.TOKEN);
             return res.status(200).send({code:200,status :"success",token:token})
            }catch(err){
                 console.log(err)
                 return res.status(422).send({code:422,status :"failure",message:err.message})
             }
    },

    /**Using Read and Update scripts **/
    async editDetails(req,res){
        let firstName = req.body.firstName
        let lastName = req.body.lastName
        if(!firstName || !lastName) return res.status(422).send({code:422,status:'failure',message:"Enter data to edit"});
        if(req.body.email) return res.status(422).send({code:422,status:'failure',message:'It cannot be edited because of uniqueness'})
        try{
            let userData = await userModule.getUser(req.user._id)
            if(userData){
                let updateData = await userModule.updateUser(req.user._id,req.body)
                return res.status(200).send({code:200,status:'success',message:"Details have been updated"})
            }
            return res.status(422).send({code:422,status:'failure'})
        }catch(err){
            return res.status(422).send({code:422,status:'failure',message:err.message})
        }
    }
}